// redux ==> flux architecture 
// web architecture MVC || FLUX || REDUX

// redux is state management tool(centarilzed state)

// glossary
// store
// reducers
// actions
// action-dispatch
// thunk
// react-redux
// provider
// connect
// mapStateToProps
// mapDispatchToProps

//1. unidirectional data flow
// data==> actions===> reducers===> store
// store can be modified by reducer only
// dispatched action are handled on reducers to update store

// MVC
// bidirectional data flow from model to controller

// Flux
// Unidirectional data flow
// actions ==> dispatcher===> store 
// multiple store
// logic reside on store

// redux
// unidirectional data flow
// actions==> reducer==>store
// single store
// state update rule reside on reducer

// react with redux
// dependecies
// redux (independant)
// react-reduc(library to connect react and redux)
// saga/thunk for middleware


// what we need redux
// store
// actions folder
// reducers folder

// react layer
// connect react app with redux
// connect component to store