import { ProductActionTypes } from './../actions/products/product.action';
const intialState = {
    isLoading: false,
    products: []
}


export const productReducer = (state = intialState, action) => {
    console.log('at reducers >>', action)
    switch (action.type) {
        case ProductActionTypes.SET_IS_LOADING:
            return {
                ...state,
                isLoading: action.payload
            }

        case ProductActionTypes.PRODUCT_RECEIVED:
            return {
                ...state,
                products: action.payload
            }

        default:
            return {
                ...state
            }
    }
}