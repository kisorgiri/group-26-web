import { combineReducers } from 'redux';
import { productReducer } from './product.red';

export const rootReducer = combineReducers({
    product: productReducer
})