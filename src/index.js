import React from 'react';
import ReactDOM from 'react-dom';
import { App } from './app.component';

ReactDOM.render(<App></App>, document.getElementById('root'));