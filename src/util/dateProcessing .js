import moment from 'moment';


function formatDate(date) {
    return moment(date).format('MM-DD-YYYY ddd');
}

function fromatTime(date) {
    return moment(date).format('hh:mm a');
}

export default {
    formatDate,
    fromatTime
}