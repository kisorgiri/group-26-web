// all http communication goes here

import axios from 'axios';
const BaseURL = process.env.REACT_APP_BASE_URL;

const http = axios.create({
    baseURL: BaseURL,
    responseType: 'json',
})

function getHeaders(isSecured) {
    let options = {
        'Content-Type': 'application/json'
    }
    if (isSecured) {
        options['Authorization'] = localStorage.getItem('token');
    }
    return options;
}

function GET(url, isSecure = false, params = {}) {
    return http.get(url, {
        headers: getHeaders(isSecure),
        params
    });

}

function POST(url, data, isSecure = false, params = {}) {
    return http.post(url, data, {
        headers: getHeaders(isSecure),
        params
    });
}

function PUT(url, data, isSecure = false, params = {}) {
    return http.put(url, data, {
        headers: getHeaders(isSecure),
        params
    });
}

function DELETE(url, isSecure = false, params = {}) {
    return http.delete(url, {
        headers: getHeaders(isSecure),
        params
    });
}

function UPLOAD(method, url, data = {}, files = []) {
    return new Promise((resolve, reject) => {
        // send xml http request to upload file
        const xhr = new XMLHttpRequest();
        const formData = new FormData();

        if (files.length) {
            files.forEach((item, i) => {
                formData.append('img', files[i], files[i].name);
            })
        }

        for (let key in data) {
            formData.append(key, data[key]);
        }

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    resolve(xhr.response)
                } else {
                    reject(xhr.response);
                }
            }
        }

        xhr.open(method, `${BaseURL}${url}?token=${localStorage.getItem('token')}`, true);
        xhr.send(formData);
    })



}

export default {
    GET,
    POST,
    PUT,
    DELETE,
    UPLOAD
}