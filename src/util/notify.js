import { toast } from 'react-toastify';

function showSuccess(msg) {
    toast.success(msg);
}

function showInfo(msg) {
    toast.info(msg);
}

function showWarning(msg) {
    toast.warning(msg);
}

function handleError(error) {
    debugger;
    let errMsg = 'Something went wrong';
    const err = error.response;
    if (err && err.data) {
        errMsg = err.data.msg;
    }
    // error
    // 1. check error
    // 2. parse error
    // 3. extract error message
    // 4. preare error message
    // 5. show them UI using toastr
    toast.error(errMsg);
}

export default {
    showSuccess,
    showInfo,
    showWarning,
    handleError
}