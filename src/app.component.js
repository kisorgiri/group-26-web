import React from 'react'
import { AppRouting } from './app.routing';
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Provider } from 'react-redux';
import { store } from './store';



export const App = () => {
    return (
        <div>
            <Provider store={store}>
                <AppRouting />
            </Provider>
            <ToastContainer />
        </div>
    )
}


// content supply garne file

// component==> basic building block of reactjs
// componet will return a html node 
// component can be functional as well as class based
// component can be stateful and stateless
// classbasedcomponent ==> stateful
// functionalcomponent==> stateless
// props==>incoming data for an component
// state==> data within a component