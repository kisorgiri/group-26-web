import React from 'react';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import { LoginComponent } from './components/auth/login/login.component';
import { Register } from './components/auth/register/register.component';
import { NavBar } from './components/common/header/header.component';
import { SideBar } from './components/common/sidebar/sidebar.component';
import { AddProduct } from './components/products/addProduct/addProduct.component';
import { EditProduct } from './components/products/editProduct/editProduct.component';
import { SearchProduct } from './components/products/searchProduct/searchProduct.component';
import ViewProduct from './components/products/viewProduct/viewProduct.component';
import ForgotPassword from './components/auth/forgotPassword/forgotPassword.component';
import { ResetPassword } from './components/auth/resetPassword/resetPassword.component';
import { MessageComponent } from './components/common/message/message.component';

const Home = (props) => {
    console.log('props in hOme >>', props)
    return <h2>Home Page</h2>
}

const Dashboard = (props) => {
    return (
        <>
            <h2>Dashboard Page</h2>
            <p>Please use side navigation menu or contact system administrator for support</p>
        </>
    )
}

const NotFound = () => {
    return (
        <>
            <h2>Page Not Found</h2>
            <img src="./images/notfound.png" alt="notfound.png" width="400px"></img>
        </>
    )
}


const ProtectedRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => (
            localStorage.getItem("isLoggedIn")
                ? <>
                    <div className="nav_bar">
                        <NavBar isLoggedIn={true} />
                    </div>
                    <div className="sidebar">
                        <SideBar isLoggedIn={true}></SideBar>
                    </div>
                    <div className="main_div">
                        <Component {...routeProps} ></Component>
                    </div>
                </>
                : <Redirect to="/"></Redirect>
        )} ></Route>
    )
}

const PublicRoute = ({ component: Component, ...rest }) => {
    return (
        <Route {...rest} render={(routeProps) => (
            <>
                <div className="nav_bar">
                    <NavBar isLoggedIn={localStorage.getItem('isLoggedIn') ? true : false} />
                </div>
                <div className="sidebar">
                    <SideBar isLoggedIn={localStorage.getItem('isLoggedIn') ? true : false}></SideBar>
                </div>

                <div className="main_div">
                    <Component {...routeProps} ></Component>
                </div>
            </>
        )} ></Route>
    )
}

export const AppRouting = (props) => {
    return (
        <>
            <BrowserRouter>
                <Switch>
                    <PublicRoute exact path="/" component={LoginComponent}></PublicRoute>
                    <PublicRoute path="/register" component={Register}></PublicRoute>
                    <PublicRoute path="/home" component={Home}></PublicRoute>
                    <PublicRoute path="/forgot_password" component={ForgotPassword}></PublicRoute>
                    <PublicRoute path="/reset_password/:id" component={ResetPassword}></PublicRoute>
                    <ProtectedRoute path="/dashboard" component={Dashboard}></ProtectedRoute>
                    <ProtectedRoute path="/add_product" component={AddProduct}></ProtectedRoute>
                    <ProtectedRoute path="/view_product" component={ViewProduct}></ProtectedRoute>
                    <ProtectedRoute path="/edit_product/:id" component={EditProduct}></ProtectedRoute>
                    <ProtectedRoute path="/messages" component={MessageComponent}></ProtectedRoute>
                    <PublicRoute path="/search_product" component={SearchProduct}></PublicRoute>
                    <PublicRoute component={NotFound} />
                </Switch>
            </BrowserRouter>
        </>
    )
}

// library react router dom
// BrowserRouter == wrapper fro entire routing
// Route==> configuration for component
// Route will add props to each component ==> history ,match, location
// Swtich==> it let only one componet to be loaded
// Link link enables us to navigate when clicking in page
// component must inside browserrouter scope to use LINK
// exact atttributes check for exact match of url


// toastr
// scope ==> within a file
// error handling part
// web storage
// localstorage
// session storage

// route le provide nagarne props ko discuss
// withRouter ==> intake props of route
// logout ==> navigate localstorage clear

// protectedRoute
// a wrapper for route registration
// if logged we can access the registered route else we will be redirected to login page

// nav bar menu will be changed automatically
// side bar menu will be changed automatically