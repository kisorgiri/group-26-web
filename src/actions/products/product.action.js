import httpClient from "../../util/httpClient";
import notify from "../../util/notify";

export const ProductActionTypes = {
    SET_IS_LOADING: 'SET_IS_LOADING',
    PRODUCT_RECEIVED: 'PRODUCT_RECEIVED'
}

// const fetch_products_ac = (params) => {
//     return function(disptach){

//     }
// }

// function fetch(params){
//     return function(disptach){

//     }
// }

export const fetch_products_ac = params => dispatch => {
    console.log('here at actions>>', params);
    dispatch({
        type: ProductActionTypes.SET_IS_LOADING,
        payload: true
    })
    // // http call
    httpClient.GET('/product', true)
        .then(response => {
            dispatch({
                type: ProductActionTypes.PRODUCT_RECEIVED,
                payload: response.data
            })
        })
        .catch(err => {
            notify.handleError(err);
        })
        .finally(() => {
            dispatch({
                type: ProductActionTypes.SET_IS_LOADING,
                payload: false
            })
        })
}

