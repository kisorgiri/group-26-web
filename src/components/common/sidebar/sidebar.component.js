import React from 'react'
import { Link } from 'react-router-dom';
import './sidebar.component.css'

export const SideBar = (props) => {
    let content = props.isLoggedIn
        ? <ul className="sidebar_list">
            <li className="sidebar_item">
                <Link to="/dashboard" >Home</Link>
            </li>
            <li className="sidebar_item">
                <Link to="/add_product" >Add Product</Link>
            </li>
            <li className="sidebar_item">
                <Link to="/view_product" >View Product</Link>
            </li>
            <li className="sidebar_item">
                <Link to="/search_product" >Search Product</Link>
            </li>
            <li className="sidebar_item">
                <Link to="/notification" >Notifications</Link>
            </li>
            <li className="sidebar_item">
                <Link to="/messages" >Message</Link>
            </li>
        </ul>
        : <ul className="sidebar_list">
            <li className="sidebar_item">
                <Link to="/" >Home</Link>
            </li>

        </ul>
    return content;


}
