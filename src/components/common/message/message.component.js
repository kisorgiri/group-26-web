import React, { Component } from 'react'
import './message.component.css';
import * as io from 'socket.io-client';
import dateUtil from './../../../util/dateProcessing ';
import notify from './../../../util/notify';

const socketURL = process.env.REACT_APP_SOCKET_URL;

export class MessageComponent extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                senderName: '',
                senderId: '',
                receiverName: '',
                receiverId: '',
                message: '',
                time: ''
            },
            messages: [],
            users: [],
            currentUser: {}
        };
    }
    componentDidMount() {
        const currentUser = JSON.parse(localStorage.getItem('user'));
        this.setState(preState => ({
            currentUser
        }), () => {
            this.runSocket();
        })
    }
    runSocket() {
        this.socket = io(socketURL);
        this.socket.emit('new-user', this.state.currentUser.username);

        this.socket.on('reply-msg', (msgData) => {
            const { messages, data } = this.state;
            messages.push(msgData);
            // swap receiver when receiving a message
            data.receiverId = msgData.senderId;
            this.setState({
                messages,
                data
            });
        })
        this.socket.on('reply-msg-own', (msgData) => {
            console.log('reply messag won >>', msgData)
            console.log('check state >>', this.state.data);
            const { messages } = this.state;
            messages.push(msgData);
            this.setState({
                messages
            });
        })
        this.socket.on('users', (users) => {
            this.setState({
                users
            })
        })
    }
    selectUser = (user) => {
        this.setState(preState => ({
            data: {
                ...preState.data,
                receiverId: user.id,
                receiverName: user.name
            }
        }))
    }

    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }))
    }

    send = e => {
        e.preventDefault();
        const { data, currentUser, users } = this.state;
        if (!data.receiverId) {
            return notify.showInfo('Please select a user to continue');
        }
        // data preparation
        data.senderName = currentUser.username;
        data.time = new Date();
        data.senderId = users.find(user => user.name === currentUser.username).id;
        this.socket.emit('new-msg', data);
        this.setState((preState) => ({
            data: {
                ...preState.data,
                message: ''
            }
        }))
    }

    render() {
        return (
            <>
                <h2>Let's Chat</h2>
                <div className="row">
                    <div className="col-md-6">
                        <ins>Messags </ins>
                        <strong> {this.state.currentUser.username}</strong>
                        <div className="chat_box">
                            <ul>
                                {this.state.messages.map((msg, i) => (
                                    <li key={i}>
                                        <p>{msg.message}</p>
                                        <h6>{msg.senderName}</h6>
                                        <small>{dateUtil.fromatTime(msg.time)}</small>
                                    </li>
                                ))}
                            </ul>
                        </div>
                        <form onSubmit={this.send} className="form-group">
                            <input type="text" name="message" placeholder="your message here" className="form-control" value={this.state.data.message} onChange={this.handleChange}></input>
                            <button className="btn btn-success" type="submit">send</button>
                        </form>
                    </div>
                    <div className="col-md-6">
                        <ins>Users</ins>
                        <div className="chat_box">
                            <ul>
                                {this.state.users.map((user, i) => (
                                    <li key={i}>
                                        <button onClick={() => this.selectUser(user)} className="btn btn-default">{user.name}</button>
                                    </li>
                                ))}
                            </ul>
                        </div>

                    </div>
                </div>

            </>
        )
    }
}
