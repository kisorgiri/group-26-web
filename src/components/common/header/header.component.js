import React from 'react';
import './header.component.css'
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router-dom';

const logout = (props) => {
    localStorage.clear();

    props.history.push('/');
    // we dont have props for component that are not registered inside route
}
const NavBarComponent = (props) => {
    let menu = props.isLoggedIn
        ? <ul className="nav_list">
            <li className="nav_item">
                <Link to="/dashboard">Home</Link>
            </li>
            <li className="nav_item">
                <button className="btn btn-success logout" onClick={() => logout(props)}>Logout</button>
            </li>

        </ul>
        : <ul className="nav_list">
            <li className="nav_item">
                <Link to="/home">Home</Link>
            </li>
            <li className="nav_item">
                <Link to="/">Login</Link>
            </li>
            <li className="nav_item">
                <Link to="/register">Register</Link>
            </li>
        </ul>
    return (
        menu
    )
}

export const NavBar = withRouter(NavBarComponent);