import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notify from '../../../util/notify';
import { Button } from './../../common/button/button.componet'

export class ResetPassword extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                password: '',
                confirmPassword: ''
            },
            error: {
                password: '',
                confirmPassword: ''
            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    componentDidMount() {
        this.id = this.props.match.params.id;
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length > 6
                        ? ''
                        : 'Weak Password'
                    : 'Required Field*';
                break;
            case 'confirmPassword':
                errMsg = this.state.data[fieldName] === this.state.data['password']
                    ? ''
                    : 'Password did not match';
                break;
            default:
                break;
        }


        this.setState(pre => ({
            error: {
                ...pre.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }
    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST(`/auth/reset-password/${this.id}`, this.state.data)
            .then(reponse => {
                notify.showInfo("Password reset successfull please login")
                this.props.history.push("/");
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <>
                <h2>Reset Password</h2>
                <p>Please choose your password wisely</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Password</label>
                    <input type="password" className="form-control" name="password" placeholder="Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>
                    <label>Confirm Password</label>
                    <input type="password" className="form-control" name="confirmPassword" placeholder="Confirm Password" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.confirmPassword}</p>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    >
                    </Button>
                </form>
            </>
        )
    }
}

