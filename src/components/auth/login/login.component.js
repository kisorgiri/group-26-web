import React from 'react';
import { Button } from '../../common/button/button.componet';
import { Link } from 'react-router-dom';
import notify from './../../../util/notify';
import httpClient from './../../../util/httpClient';

export class LoginComponent extends React.Component {

    constructor() {
        super(); //parent class constructor call
        // initial block
        this.state = {
            data: {
                username: '',
                password: ''
            }, error: {
                username: '',
                password: ''
            },
            remember_me: false,
            isSubmitting: false,
            isValidForm: false
        }
        // console.log('this.props >>', this.props);
    }

    componentDidMount() {
        if (localStorage.getItem('remember_me')) {
            this.props.history.push('/dashboard/random');
        }
    }

    handleChange = (e) => {
        // console.log('on change >>', e.target);
        let { type, name, value, checked } = e.target;
        if (type === 'checkbox') {
            value = checked;
        }
        this.setState((preState) => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(name) {
        let errMsg = this.state.data[name]
            ? ''
            : `${name} is required`

        this.setState((preState) => ({
            error: {
                ...preState.error,
                [name]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })

        httpClient
            .POST(`/auth/login`, this.state.data)
            .then(response => {
                console.log('response >>', response)
                notify.showSuccess(`Welcome ${response.data.user.username}`);
                localStorage.setItem('token', response.data.token);
                localStorage.setItem('user', JSON.stringify(response.data.user));
                localStorage.setItem('isLoggedIn', true);
                this.props.history.push('/dashboard');
                // check respose
                // add data to localstorage
                // navigate to dashboard
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            });
    }
    // important and mandatory method is render
    render() {
        // render must return a single html node
        // view logic are defined here


        return (
            <div>
                <h2>Login</h2>
                <p>Please Login to Start Your Session</p>
                <form className="form-group" onSubmit={this.handleSubmit.bind(this)}>
                    <label htmlFor="username">Username</label>
                    <input className="form-control" type="text" placeholder="Username" name="username" onChange={this.handleChange} id="username"></input>
                    <p className="error">{this.state.error.username}</p>
                    <label htmlFor="password">Password</label>
                    <input className="form-control" type="password" placeholder="Password" name="password" onChange={this.handleChange} id="password"></input>
                    <p className="error">{this.state.error.password}</p>
                    <input type="checkbox" onChange={this.handleChange} name="remember_me"></input>
                    <label>Remember Me</label>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                        label="Login"
                        disabledLabel="Logining in..."
                    ></Button>
                </form>
                <p>Don't have an Account?</p>
                <p>
                    <Link to="/register">  Register here</Link>
                    <Link className="float_right" to="/forgot_password">forgot password?</Link>
                </p>

            </div>
        )
    }
}