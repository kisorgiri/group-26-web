import React, { Component } from 'react';
import { Button } from '../../common/button/button.componet';
import { Link } from 'react-router-dom';
import notify from '../../../util/notify';
import httpClient from '../../../util/httpClient';


const defaultForm = {
    name: '',
    email: '',
    username: '',
    password: '',
    gender: '',
    dob: '',
    phoneNumber: '',
    temp_address: '',
    permanent_address: ''
}

export class Register extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm,
            },
            isSubmitting: false,
            isValidForm: false
        }
    }

    componentDidMount() {
        // console.log('at third');
        // console.log('initial condition are applied here');
        // console.log('this props >>', this.props)
    }

    componentDidUpdate(preProps, PreState) {
        // either props change or state change
        // console.log('component did update ');
    }

    componentWillUnmount() {
        console.log('component is destroyed');
        // cancle all the subription
        // cancle all asyc call
    }
    handleChange = e => {
        let { type, name, value } = e.target;
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })

    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'username':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : 'required field*';
                break;
            case 'password':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].length < 6
                        ? 'weak password'
                        : ''
                    : 'required field*'
                break;

            case 'email':
                errMsg = this.state.data[fieldName]
                    ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                        ? ''
                        : 'Invalid Email'
                    : 'required field*';
                break;

            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {

            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        console.log('this.state.', this.state);
        // http call 
        // data ==== this.state.data
        this.setState({
            isSubmitting: true
        })
        httpClient
            .POST(`/auth/register`, this.state.data)
            .then((response) => {
                console.log('data is >>', response)
                notify.showSuccess("Registration successfull please login!");
                this.props.history.push('/');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        console.log('render at second');
        return (
            <div>
                <h2>Register</h2>
                <p>Please provide necessary details to register</p>
                <form onSubmit={this.handleSubmit} className="form-group" noValidate>
                    <label>Name</label>
                    <input type="text" name="name" placeholder="Name" className="form-control" onChange={this.handleChange}></input>
                    <label>Email</label>
                    <input type="email" name="email" placeholder="Email" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>

                    <label>Username</label>
                    <input type="text" name="username" placeholder="Username" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.username}</p>
                    <label>Password</label>
                    <input type="text" name="password" placeholder="Password" className="form-control" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.password}</p>

                    <label>Phone Number</label>
                    <input type="number" name="phoneNumber" placeholder="Phone Number" className="form-control" onChange={this.handleChange}></input>
                    <label>Gender</label>
                    <input type="text" name="gender" placeholder="Gender" className="form-control" onChange={this.handleChange}></input>
                    <label>Permanent Address</label>
                    <input type="text" name="permanent_address" placeholder="Permanent Address" className="form-control" onChange={this.handleChange}></input>
                    <label>Temporary Address</label>
                    <input type="text" name="temp_address" placeholder="Temporary Address" className="form-control" onChange={this.handleChange}></input>
                    <label>Date Of Birth</label>
                    <input type="date" name="dob" className="form-control" onChange={this.handleChange}></input>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    ></Button>

                </form>
                <p>
                    ALready Registered?
                </p>
                <p>Back to
                    <Link to="/"> login</Link>
                </p>
            </div>
        )
    }
}

// life cyle
// initital phase ==> fetch data, check props
// update phase ==> check difference prev and current props and state
// destroy ==> memory leakage prevent 

// initial phase==> componentDidMount();
// update phase==> componentDidUpdate();
// destroy phase ==>componentWillUnMount();
