import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notify from '../../../util/notify';
import { Button } from './../../common/button/button.componet'

export default class ForgotPassword extends Component {
    constructor() {
        super();
        this.state = {
            data: {
                email: ''
            },
            error: {
                email: ''
            },
            isSubmitting: false,
            isValidForm: false
        }
    }
    handleChange = e => {
        const { name, value } = e.target;
        this.setState(preState => ({
            data: {
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    validateForm(fieldName) {
        let errMsg = this.state.data[fieldName]
            ? this.state.data[fieldName].includes('@') && this.state.data[fieldName].includes('.com')
                ? ''
                : 'Invalid Email'
            : 'Required Field*';

        this.setState(pre => ({
            error: {
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }
    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        httpClient.POST('/auth/forgot-password', this.state.data)
            .then(reponse => {
                notify.showInfo("Password reset link sent to your email please check inbox")
                this.props.history.push("/");
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <>
                <h2>Forgot Password</h2>
                <p>Please enter your Email Address to reset your password</p>
                <form className="form-group" onSubmit={this.handleSubmit} noValidate>
                    <label>Email</label>
                    <input type="text" className="form-control" name="email" placeholder="Email Address" onChange={this.handleChange}></input>
                    <p className="error">{this.state.error.email}</p>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isDisabled={!this.state.isValidForm}
                    >
                    </Button>
                </form>
            </>
        )
    }
}

