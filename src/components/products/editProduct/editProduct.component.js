import React, { Component } from 'react'
import { ProductForm } from './../prodcutForm/productForm.component';
import httpClient from './../../../util/httpClient'
import notify from './../../../util/notify'
import { Loader } from './../../common/loader/loader.component'

export class EditProduct extends Component {

    constructor() {
        super();
        this.state = {
            product: '',
            isLoading: false
        };
    }

    componentDidMount() {
        const productId = this.props.match.params['id'];
        this.setState({
            isLoading: true
        })
        httpClient
            .GET(`product/${productId}`, true)
            .then(response => {
                this.setState({
                    product: response.data
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
            .finally(() => {
                this.setState({
                    isLoading: false
                })
            })

    }

    edit = (product, files) => {
        console.log('product >>', product);
        console.log('vendor >>', product.vendor);
        let data = {
            ...product,
            vendor: product.vendor._id
        }
        httpClient
            .UPLOAD('PUT', `/product/${product._id}`, data, files)
            .then(response => {
                this.props.history.push('/view_product');
                notify.showInfo("Product Updated Successfully");
            })
            .catch(err => {
                notify.handleError(err);
            })
    }

    render() {
        let content = this.state.isLoading
            ? <Loader></Loader>
            : <ProductForm title="Edit Product" product={this.state.product} submitCallback={this.edit}></ProductForm>

        return (
            content
        )
    }
}
