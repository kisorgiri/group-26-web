import React, { Component } from 'react'
import { Button } from '../../common/button/button.componet'
const ImgURL = process.env.REACT_APP_IMAGE_URL;

const defaultForm = {
    name: '',
    description: '',
    category: '',
    brand: '',
    size: '',
    color: '',
    price: '',
    modelNo: '',
    tags: '',
    offers: '',
    discountedItem: false,
    discountType: '',
    discountValue: '',
    warrentyStatus: '',
    warrentyPeroid: '',
}

export class ProductForm extends Component {

    constructor() {
        super();
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            filesToUpload: [],
            isValidForm: false
        };
    }

    componentDidMount() {
        console.log('props >>', this.props);
        if (this.props.product) {
            this.setState({
                data: {
                    ...defaultForm,
                    ...this.props.product,
                    discountedItem: this.props.product.discount ? this.props.product.discount.discountedItem : false,
                    discountType: this.props.product.discount ? this.props.product.discount.discountType : '',
                    discountValue: this.props.product.discount ? this.props.product.discount.discountValue : '',
                    tags: (this.props.product.tags || []).toString(),
                    offers: (this.props.product.offers || []).toString(),
                }
            })
        }
    }

    handleChange = e => {
        let { name, type, value, checked, files } = e.target;
        if (type === 'checkbox') {
            value = checked;
        }
        if (type === 'file') {
            const { filesToUpload } = this.state;
            filesToUpload.push(files[0])
            this.setState({
                filesToUpload
            })
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }

    validateForm(fieldName) {
        let errMsg;
        switch (fieldName) {
            case 'category':
                errMsg = this.state.data[fieldName]
                    ? ''
                    : `${fieldName} is required*`
                break;

            default:
                break;
        }

        this.setState(preState => ({
            error: {
                ...preState.error,
                [fieldName]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);

            this.setState({
                isValidForm: errors.length === 0
            });
        })
    }


    handleSubmit = (e) => {
        e.preventDefault();
        this.props.submitCallback(this.state.data, this.state.filesToUpload);
    }

    render() {
        let warrentyContent = this.state.data.warrentyStatus
            ? <>
                <label>Warrenty Peroid</label>
                <input className="form-control" type="text" value={this.state.data.warrentyPeroid} placeholder="Warrenty Peroid" name="warrentyPeroid" onChange={this.handleChange}></input>
            </>
            : ''
        return (
            <>
                <h2>{this.props.title}</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Name</label>
                    <input className="form-control" type="text" value={this.state.data.name} placeholder="Name" name="name" onChange={this.handleChange}></input>
                    <label>Description</label>
                    <input className="form-control" type="text" value={this.state.data.description} placeholder="Description" name="description" onChange={this.handleChange}></input>
                    <label>Category</label>
                    <input className="form-control" type="text" value={this.state.data.category} placeholder="Category" name="category" onChange={this.handleChange}></input>
                    <label>Brand</label>
                    <input className="form-control" type="text" value={this.state.data.brand} placeholder="Brand" name="brand" onChange={this.handleChange}></input>
                    <label>Price</label>
                    <input className="form-control" type="number" value={this.state.data.price} placeholder="Price" name="price" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input className="form-control" type="text" value={this.state.data.color} placeholder="Color" name="color" onChange={this.handleChange}></input>
                    <label>Size</label>
                    <input className="form-control" type="text" value={this.state.data.size} placeholder="Size" name="size" onChange={this.handleChange}></input>
                    <label>Model No</label>
                    <input className="form-control" type="text" value={this.state.data.modelNo} placeholder="Model No" name="modelNo" onChange={this.handleChange}></input>
                    <label>Offers</label>
                    <input className="form-control" type="text" value={this.state.data.offers} placeholder="Offers" name="offers" onChange={this.handleChange}></input>
                    <label>Tags</label>
                    <input className="form-control" type="text" value={this.state.data.tags} placeholder="Tags" name="tags" onChange={this.handleChange}></input>
                    <input type="checkbox" name="warrentyStatus" checked={this.state.data.warrentyStatus} onChange={this.handleChange}></input>
                    <label>Warrenty</label>
                    <br />
                    {warrentyContent}
                    <input type="checkbox" name="discountedItem" checked={this.state.data.discountedItem} onChange={this.handleChange}></input>
                    <label>Discounted Item</label>
                    <br />
                    {this.state.data.discountedItem && (
                        <>
                            <label>Discount Type</label>
                            <select name="discountType" value={this.state.data.discountType} className="form-control" onChange={this.handleChange}>
                                <option value="">(Select Type)</option>
                                <option value="percentage">Percentage</option>
                                <option value="qunatity">Quantity</option>
                                <option value="value">Value</option>
                            </select>
                            <label>Discount Value</label>
                            <input className="form-control" type="text" value={this.state.data.discountValue} placeholder="Discout Value" name="discountValue" onChange={this.handleChange}></input>
                            <br />
                        </>
                    )}
                    {
                        this.props.product && this.props.product.images && this.props.product.images.length && (
                            <>
                                <label>Previous Image</label>
                                <br />
                                <img src={`${ImgURL}${this.props.product.images[0]}`} alt="productImage.png" width="400px"></img>
                                <br></br>
                            </>
                        )
                    }
                    <label>Choose File</label>
                    <input type="file" className="form-control" name="image" onChange={this.handleChange}></input>
                    <br></br>
                    <Button
                        isSubmitting={this.props.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    />

                </form>
            </>
        )
    }
}

