import React, { Component } from 'react'
import { ProductForm } from '../prodcutForm/productForm.component'
import httpClient from './../../../util/httpClient'
import notify from './../../../util/notify'

export class AddProduct extends Component {
    constructor() {
        super();
        this.state = {
            isSubmitting: false
        };
    }

    add = (data, files) => {
        this.setState({
            isSubmitting: true
        })
        httpClient
            .UPLOAD('POST', '/product', data, files)
            .then(response => {
                notify.showSuccess('Product Added Successfully');
                this.props.history.push('/view_product');
            })
            .catch(err => {
                notify.handleError(err);
                this.setState({
                    isSubmitting: false
                })
            })
    }

    render() {
        return (
            <div>
                <ProductForm isSubmitting={this.state.isSubmitting} title="Add Product" submitCallback={this.add} ></ProductForm>
            </div>
        )
    }
}
