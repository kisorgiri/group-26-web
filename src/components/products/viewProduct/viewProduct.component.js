import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Loader } from '../../common/loader/loader.component';
import httpClient from './../../../util/httpClient'
import notify from './../../../util/notify'
import dateUtil from './../../../util/dateProcessing ';
import { connect } from 'react-redux';
import { fetch_products_ac } from './../../../actions/products/product.action'

const ImgURL = process.env.REACT_APP_IMAGE_URL;

class ViewProduct extends Component {
    constructor() {
        super();
        this.state = {
            isLoading: false,
            products: []
        }
    }

    componentDidMount() {
        console.log('check all props >>', this.props);
        if (this.props.incomingData) {
            this.setState({
                products: this.props.incomingData
            });
        } else {
            this.props.fetch();
        }
    }

    removeProduct = (id, index) => {
        //eslint-disable-next-line no-restricted-globals
        // let confirmation = confrim('Are you sure to remove?');
        // if (confirmation) {
        httpClient.DELETE(`/product/${id}`, true)
            .then(response => {
                notify.showInfo('Product Removed')
                const { products } = this.state;
                products.splice(index, index);
                this.setState({
                    products
                })
            })
            .catch(err => {
                notify.handleError(err);
            })
        // }
    }

    editProduct = (id) => {
        this.props.history.push(`/edit_product/${id}`);
    }

    render() {
        let content = this.props.isLoading
            ? <Loader></Loader>
            : <>
                <table className="table">
                    <thead>
                        <tr>
                            <th>S.N</th>
                            <th>Name</th>
                            <th>Category</th>
                            <th>price</th>
                            <th>Created Date</th>
                            <th>Created Time</th>
                            <th>Images</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.products.map((item, i) => (
                            <tr key={item._id}>
                                <td>{i + 1}</td>
                                <td>
                                    <Link to={`product_details/${item._id}`}>{item.name}</Link>
                                </td>
                                <td>{item.category}</td>
                                <td>{item.price}</td>
                                <td>{dateUtil.formatDate(item.createdAt)}</td>
                                <td>{dateUtil.fromatTime(item.createdAt)}</td>
                                <td>
                                    <img src={`${ImgURL}${item.images[0]}`} alt="image.png" width="200px"></img >
                                </td>
                                <td>
                                    <button className="btn btn-info" onClick={() => this.editProduct(item._id)}>edit</button>
                                    <button className="btn btn-danger" onClick={() => this.removeProduct(item._id, i)}>delete</button>
                                </td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </>
        return (
            <>
                <h2>View Product</h2>
                {this.props.incomingData && (
                    <button className="btn btn-success" onClick={this.props.resetSearch}>Search Again</button>
                )}
                {content}

            </>
        )
    }
}

// what component will intake as an props
const mapStateToProps = centralStore => ({
    isLoading: centralStore.product.isLoading,
    products: centralStore.product.products
})

// what component will trigger
const mapDispatchToProps = dispatch => ({
    fetch: () => dispatch(fetch_products_ac({ pageCount: 10 }))
})

export default connect(mapStateToProps, mapDispatchToProps)(ViewProduct)