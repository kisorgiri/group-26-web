import React, { Component } from 'react'
import httpClient from '../../../util/httpClient';
import notify from '../../../util/notify';
import ViewProduct from '../viewProduct/viewProduct.component';
import { Button } from './../../common/button/button.componet';
const defaultForm = {
    name: '',
    category: '',
    brand: '',
    minPrice: '',
    maxPrice: '',
    color: '',
    fromDate: '',
    toDate: '',
    mutipleDateRange: false,
    tags: '',
    discountedItem: '',
    warrentyStatus: ''
}
export class SearchProduct extends Component {
    constructor() {
        super()
        this.state = {
            data: {
                ...defaultForm
            },
            error: {
                ...defaultForm
            },
            allProducts: [],
            categories: [],
            names: [],
            searchResult: [],
            isSubmitting: false,
            isValidForm: false
        };
    }

    componentDidMount() {
        httpClient.POST('/product/search', {})
            .then(response => {
                console.log('resposne is >>', response);
                let categories = [];
                response.data.forEach(item => {
                    if (categories.indexOf(item.category) === -1) {
                        categories.push(item.category);
                    }
                });
                this.setState({
                    allProducts: response.data,
                    categories
                })
            })
            .catch(err => notify.handleError(err));
    }

    handleChange = e => {
        let { name, value, type, checked } = e.target;
        if (type === 'checkbox') {
            value = checked;
        }
        if (name === 'category') {
            this.buildNameOptions(value);
        }
        this.setState(preState => ({
            data: {
                ...preState.data,
                [name]: value
            }
        }), () => {
            this.validateForm(name);
        })
    }
    buildNameOptions(categoryName) {
        let names = this.state.allProducts.filter(item => item.category === categoryName);
        this.setState({
            names
        })
    }

    validateForm(name) {
        let errMsg;
        switch (name) {
            case 'category':
                errMsg = this.state.data[name]
                    ? ''
                    : 'Required Field*'
                break;
            default:
                break;
        }
        this.setState(preState => ({
            error: {
                ...preState.error,
                [name]: errMsg
            }
        }), () => {
            const errors = Object
                .values(this.state.error)
                .filter(err => err);
            this.setState({
                isValidForm: errors.length === 0
            })
        })
    }

    handleSubmit = e => {
        e.preventDefault();
        this.setState({
            isSubmitting: true
        })
        const { data } = this.state;
        if (!data.mutipleDateRange) {
            data.toDate = data.fromDate;
        }
        httpClient.POST('/product/search', this.state.data)
            .then(response => {
                if (!response.data.length) {
                    notify.showInfo('No any Product matched your search query')
                }
                this.setState({
                    searchResult: response.data
                })
            })
            .catch(err => notify.handleError(err))
            .finally(() => {
                this.setState({
                    isSubmitting: false
                })
            })
    }
    resetSearch = () => {
        this.setState({
            searchResult: [],
            data: { ...defaultForm }
        })
    }

    render() {
        let searchContent = this.state.searchResult.length
            ? <ViewProduct incomingData={this.state.searchResult} resetSearch={this.resetSearch}></ViewProduct>
            : <>
                <h2>Search Product</h2>
                <form className="form-group" onSubmit={this.handleSubmit}>
                    <label>Category</label>
                    <select name="category" onChange={this.handleChange} className="form-control">
                        <option value="">(Select Category)</option>
                        {
                            this.state.categories.map((item, i) => (
                                <option key={i} value={item}>{item}</option>
                            ))
                        }
                    </select>
                    {this.state.data.category && (
                        <>
                            <label>Name</label>
                            <select name="name" onChange={this.handleChange} className="form-control">
                                <option value="">(Select Name)</option>
                                {
                                    this.state.names.map(item => (
                                        <option key={item._id} value={item.name}>{item.name}</option>
                                    ))
                                }
                            </select>
                        </>
                    )}
                    <label>Brand</label>
                    <input className="form-control" type="text" value={this.state.data.brand} placeholder="Brand" name="brand" onChange={this.handleChange}></input>
                    <label>Color</label>
                    <input className="form-control" type="text" value={this.state.data.color} placeholder="Color" name="offers" onChange={this.handleChange}></input>
                    <label>Min Price</label>
                    <input className="form-control" type="number" value={this.state.data.minPrice} placeholder="Min Price" name="minPrice" onChange={this.handleChange}></input>
                    <label>Max Price</label>
                    <input className="form-control" type="number" value={this.state.data.maxPrice} placeholder="Max Price" name="maxPrice" onChange={this.handleChange}></input>
                    <label>Select Date</label>
                    <input className="form-control" type="date" value={this.state.data.fromDate} name="fromDate" onChange={this.handleChange}></input>
                    <input type="checkbox" name="mutipleDateRange" onChange={this.handleChange}></input>
                    <label>Multiple Date Range</label>
                    <br />

                    {this.state.data.mutipleDateRange && (
                        <>
                            <label>To Date</label>
                            <input className="form-control" type="date" value={this.state.data.toDate} name="toDate" onChange={this.handleChange}></input>
                        </>
                    )}
                    <label>Tags</label>
                    <input className="form-control" type="text" value={this.state.data.tags} placeholder="Tags" name="tags" onChange={this.handleChange}></input>
                    <input type="checkbox" name="warrentyStatus" checked={this.state.data.warrentyStatus} onChange={this.handleChange}></input>
                    <label>Warrenty Item</label>
                    <br />
                    <Button
                        isSubmitting={this.state.isSubmitting}
                        isValidForm={this.state.isValidForm}
                    >

                    </Button>
                </form>
            </>
        return searchContent;
    }
}
